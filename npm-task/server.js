
const {urlencoded, json} = require('express')
const express = require('express')
const router = require('./routes')
// let posts=require('./models/user.json')

// const path = require('path')
const app =express()
const port = 3000

// app.get('/api/v1/posts', (req,res)=> {
//     res.status(200).json(posts)
// })
// template engine setup

app.use('/static', express.static(__dirname + '/public'))
// app.use(express.static("public"))
app.use(json())
app.use(urlencoded({extended : true}))
app.set('view engine', 'ejs')
// app.set('views', path.join(__dirname, 'views'))

app.use(router)


// app.get('/', (req, res)=> {
//     // res.send('halo ini menggunakan express.js!')
//     res.render('home.ejs')
// })

app.listen(port, ()=> {
    console.log('Aplikasi ini running di port: ' + port)
})

// var os = require('os')
// console.log(os.freemem())

// var lingkaran= require('./local_module')

// lingkaran.luaslingkaran(3)
// lingkaran.kelilinglingkaran(3)